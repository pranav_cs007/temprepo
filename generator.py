from flask import Flask,render_template
from werkzeug import cached_property
import markdown 
import os

POSTS_FILE_EXTENSION = '.md'

app=Flask(__name__)

class Blog(object):
	def __init__(self,app,root_dir='',file_ext=POSTS_FILE_EXTENSION)
		self.root_dir=root_dir
		self.file_ext=file_ext
		self._app=app
		self._cache={}
		self._initialize_cache()
	@property 
	def posts(self):
		return self._cache.values()

	def get_posts_or_404(self,path):
		try:
			return self._cache[path]
		except KeyError:
			abort(404)

	def _initialize_cache(self):
		for (root,dirpaths,filepaths) in os.walk(self.root_dir):
			for filepath in filepaths:
				filename,ext=os.path.splitext(filepath)
				if ext==self.file_ext:
					path=os.path.join(root,filepath).replace(self.root_dir,'')
					post=Post(path,root_dir=self.root_dir)
					self._cache[post.urlpath]=post						

class Post(object):
       def __init__(self, path):
               self.path = path

       @cached_property
       def html(self):
               with open(self.path, 'r') as fin:
                       content = fin.read().split('\n\n',1)[1].strip()
               return markdown.markdown(content)


        def _initialize_metadata(self):
        	content=''
        	with open(self.path , 'r') as fin:
        		for line in fin:
        			if not line.strip():
        				break
        			content+=line
        	self.__dict__.update(yaml.load(content))				       

@app.route('/')
def index():
	return render_template('index.html',post=blog.posts)


@app.route('/blog/<path:path>')
def post(path):
#	path= os.path.join('posts' , path + POSTS_FILE_EXTENSION)
	post=blog.get_posts_or_404(path)
	return render_template('post.html', post=post)


if __name__ == '__main__':
		app.run(port=8000, debug=True)

			